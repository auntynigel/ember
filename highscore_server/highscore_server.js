var http = require('http');

var highscores = [{id: 0, type: 'highscore',attributes: {name: 'Fred', score: 12}}, {id: 1, type: 'highscore',attributes: {name: 'Wilma', score: 13}}];
var next_id = 2;

var server = http.createServer(function(request, response){
    if(request.url == '/highscores'){
        /** set response headers to allow access from anywhere **/
        response.setHeader('Content-Type', 'application/json');
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Allow-Headers', 'content-type');
        if(request.method = 'GET'){
            /** handle GET requests, returns scores **/
            response.write(JSON.stringify({
                data: highscores
            }));
            response.end();
        } else if(request.method == 'POST'){
            /** handles POST request, posts scores to array **/
            data = [];
            request.on('data', function(chunk){
                /** POST submissions are complex, data "chunks" are stored in temp var 'data' **/
                data.push(chunk);
            });
            request.on('end', function(){
                /** when data completed add to highscores variable **/
                var highscore = JSON.parse(Buffer.concat(data).toString());
                highscore.data.id = next_id;
                next_id = next_id + 1;
                highscores.push(highscore.data);
                /** highscores sort by score **/
                highscores.sort(function(a, b){
                    return b.attributes.score - a.attributes.score;
                });
                response.write(JSON.stringify(highscore));
                response.end();
            });
        } else {
            response.end();
        }
    } else {
        response.end();
    }
});

server.listen(4201);